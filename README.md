##*Teste Sicredi*

Aplicativos necessários para teste:

1. Java8.
2. Eclipse.
3. Postman.

*Obs.: A versão do Eclipse utilizada foi 2020-03 (4.15.0).*

---

**Incluir pauta**

URL: (http://localhost:8080/api/pauta)

Método: POST

```Javascript
{
  "descricao": "VOTACAO DE AGOSTO",
  "dataInicio": "2020-09-28T20:33",
  "dataFim": "2020-09-28T20:40"
}
```
Retorno:

```Javascript
{
	"id": 2, 
	"mensagem":"Pauta incluida com sucesso."
}
```
---

**Incluir Voto**

URL: (http://localhost:8080/api/voto)

Método: POST

```Javascript
{
  "clienteId": {
    "id": 1
  },
  "pautaId": {
    "id": "2"
  },
  "voto": true
}
```
---

**Consultar Resultado da Pauta**

URL: (http://localhost:8080/api/voto?pauta=100)

Obs.: Passar o id da pauta obita no retorno do post após incluir a pauta

Método: GET

Retorno:

```Javascript
{
	"pauta": "100", 
	"votos_sim":"2", 
	"votos_nao":"2"
}
```
---
**Passos a passo do teste:**

<ul>
	<li>Incluir a pauta.</li>
	<li>Incluir o voto.</li>
	<li>Consultar resultado.</li>
</ul>

---

 **Validações executadas:**

<ul>
 <li>O hibernate valida se o CPF é válido.</li>
 <li>O CPF é verificado externamente, se é valido e está apto a votar permite o voto.</li>
 <li>O datetime dos campos dataInicio e dataFim da tabela Pauta são validados pelo datetime do pc, ou seja, será permitido apenas votar se o time de votação estiver entre a dataInicio e dataFim.</li>
 <li>Caso o cliente já tenho votado na pauta não poderá votar novamente.</li>
</ul>
 