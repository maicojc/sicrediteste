package com.sicrediTeste;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.github.javafaker.Faker;
import com.sicrediTeste.Model.Cliente;
import com.sicrediTeste.Repository.ClienteRepository;
import com.sicrediTeste.Util.GeraCpfCnpj;

@Component
public class Loading implements CommandLineRunner{
	
	@Autowired
	private ClienteRepository repoCliente;
	
	@Override
	public void run(String... args) throws Exception{
		
		
		//ArrayList<Cliente> clientes = 
		
		
		
		Faker faker = new Faker(new Locale("pt-BR"));
				
	//	ArrayList<Cliente> clientes = new ArrayList<Cliente>(); 
		for(int i = 0; i < 100; i++){ 
			
			Cliente cliente = new Cliente();
			
			String cpf = GeraCpfCnpj.geraCPF();
			
			@SuppressWarnings("deprecation")
			Long l= new Long(i);
			//org.webjars.bower
			cliente.setId(l);
			cliente.setNome(faker.name().name());
			cliente.setCpf(cpf);
			
			//clientes.add(cliente);
			
			repoCliente.save(cliente);
			 
		} /*try { 
			dao.saveOrUpdateAll(l); 
			JOptionPane.showMessageDialog(null, "Deu certo!");
		} catch (Exception ex) { 
			Logger.getLogger(CadPlanejamentoHoras.class.getName()).log(Level.SEVERE, null, ex); 
		}		
				
		cliente.setId(1L);
		cliente.setNome("Maria José");
		cliente.setCpf("00082355037");*/
		
		//repoCliente.saveAll(clientes);
		
	}

}
