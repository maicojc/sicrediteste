package com.sicrediTeste.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.sicrediTeste.Model.Cliente;

public interface ClienteRepository extends JpaRepository<Cliente, Long> {

	@Query("select c.cpf from Cliente c where c.id=:id")
	String buscaCPFCliente(Long id);

}
