package com.sicrediTeste.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.sicrediTeste.Model.Pauta;

public interface PautaRepository extends JpaRepository<Pauta, Long>{

	boolean findByDataFim(boolean before);

}
