package com.sicrediTeste.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.sicrediTeste.Model.Cliente;
import com.sicrediTeste.Model.Pauta;
import com.sicrediTeste.Model.Votos;

public interface VotosRepository extends JpaRepository<Votos, Long>{

	@Query("select count(v) from Votos v where v.clienteId=:clienteId and v.pautaId=:pautaId")
	int validaSeVotou(@Param("clienteId") Cliente clienteId, @Param("pautaId") Pauta pautaId);

	@Query("SELECT v from Votos v where v.pautaId=:pautaId")
	public List<Votos> totalPautaId(@Param("pautaId") Pauta pautaId);

}
