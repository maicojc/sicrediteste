package com.sicrediTeste.Model;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;


@Entity
public class Pauta {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Long id;
	
	private String descricao;
	
	private LocalDateTime dataInicio; 
	
	private LocalDateTime dataFim; 
	
	@JsonIgnore
	@OneToMany(
			mappedBy = "pautaId", 
			cascade = CascadeType.ALL,
			orphanRemoval = true
	)
	private List<Votos> votos = new ArrayList<Votos>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public LocalDateTime getDataInicio() {
		return dataInicio;
	}

	public LocalDateTime getDataFim() {
		return dataFim;
	}

	public void setDataFim(LocalDateTime dataFim) {
		this.dataFim = dataFim;
	}

	public void setDataInicio(LocalDateTime dataInicio) {
		System.out.println("data: " + dataInicio);
		this.dataInicio = dataInicio;
	}

	public List<Votos> getVotos() {
		return votos;
	}

	public void setVotos(List<Votos> votos) {
		this.votos = votos;
	}
}
