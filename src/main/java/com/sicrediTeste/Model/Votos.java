package com.sicrediTeste.Model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Votos {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Long id;
	
	private Boolean voto;
	
	@ManyToOne
	@JoinColumn(name = "id_pauta_fk")
	private Pauta pautaId;
	
	@ManyToOne
	@JoinColumn(name = "id_cliente_fk")
	private Cliente clienteId;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Boolean getVoto() {
		return voto;
	}

	public void setVoto(Boolean voto) {
		this.voto = voto;
	}

	public Cliente getClienteId() {
		return clienteId;
	}

	public void setClienteId(Cliente clienteId) {
		this.clienteId = clienteId;
	}

	public Pauta getPautaId() {
		return pautaId;
	}

	public void setPautaId(Pauta pautaId) {
		this.pautaId = pautaId;
	}
	
}
