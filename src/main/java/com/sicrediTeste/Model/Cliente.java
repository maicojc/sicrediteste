package com.sicrediTeste.Model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import org.hibernate.validator.constraints.br.CPF;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Cliente {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Long id;
	
	private String nome;
	
	@CPF(message = "CPF inválido")
	private String cpf;
	
	@JsonIgnore
	@OneToMany(
			mappedBy = "clienteId", 
			cascade = CascadeType.ALL,
			orphanRemoval = true
	)
	private List<Votos> votos = new ArrayList<Votos>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public List<Votos> getVotos() {
		return votos;
	}

	public void setVotos(List<Votos> votos) {
		this.votos = votos;
	}
}
