package com.sicrediTeste.API;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sicrediTeste.Model.Pauta;
import com.sicrediTeste.Repository.PautaRepository;

@RestController
@RequestMapping("/api/pauta")
public class PautaAPI {

	@Autowired
	private PautaRepository repoPauta;
	
	String retornoAPI = "";
	
	@PostMapping
	public ResponseEntity<?> incluir(@RequestBody Pauta pauta, BindingResult result){
		
		ResponseEntity<?> retorno = null;
		
		if(result.hasErrors()) {
			
			List<FieldError> erros = result.getFieldErrors();
			retorno = new ResponseEntity<>(erros, HttpStatus.BAD_REQUEST);
			
		} else {
			
			repoPauta.save(pauta);
				
			retornoAPI = "{\"id\": "+pauta.getId().toString()+", \"mensagem\":\"Pauta incluida com sucesso.\"}";
				
			retorno = new ResponseEntity<>(retornoAPI,HttpStatus.CREATED);
		}
		
		return retorno;
	}
}
