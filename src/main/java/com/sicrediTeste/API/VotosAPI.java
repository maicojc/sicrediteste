package com.sicrediTeste.API;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.time.LocalDateTime;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.sicrediTeste.Model.Votos;
import com.sicrediTeste.Repository.ClienteRepository;
import com.sicrediTeste.Repository.PautaRepository;
import com.sicrediTeste.Repository.VotosRepository;

@RestController
@RequestMapping("/api/voto")
public class VotosAPI {

	@Autowired
	private VotosRepository repoVoto;
	
	@Autowired
	private ClienteRepository repoCliente;
	
	@Autowired
	private PautaRepository repoPauta;
	
	String retornoAPI = "";
	
	@GetMapping(params = {"pauta"})
	public ResponseEntity<?> votacao(@RequestParam String pauta)
	{
		List<Votos> votacao = repoVoto.totalPautaId(repoPauta.findById(Long.parseLong(pauta)).get());
	
		ResponseEntity<?> retorno = null;
		
		if(votacao.isEmpty()) {
			
			retorno = new ResponseEntity<>(HttpStatus.NO_CONTENT);
			
		} else {
			
			String getPauta = "";
		    int votoSim = 0;
			int votoNao = 0;
			
			for(Votos n: votacao) {
				getPauta = n.getPautaId().getId().toString();
			    
			    if(n.getVoto() == true) {votoSim++;} else {votoNao++;}
			}
			
			retornoAPI = "{\"pauta\": \""+getPauta+"\", \"votos_sim\":\""+votoSim+"\", \"votos_nao\":\""+votoNao+"\"}";
			
			retorno = new ResponseEntity<>(retornoAPI, HttpStatus.OK);
		}
		
		return retorno;
	}
	
	@PostMapping
	public ResponseEntity<?> incluir(@Valid @RequestBody Votos voto, BindingResult result){
		
		ResponseEntity<?> retorno = null;
		
		if(result.hasErrors()) {
			
			List<FieldError> erros = result.getFieldErrors();
			retorno = new ResponseEntity<>(erros, HttpStatus.BAD_REQUEST);
			
		} else {
			
			System.out.println("cpf: "+repoCliente.buscaCPFCliente(voto.getClienteId().getId()));
			
		        try {

		            URL url = new URL("https://user-info.herokuapp.com/users/"+repoCliente.buscaCPFCliente(voto.getClienteId().getId()));
		            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		            conn.setRequestMethod("GET");
		            conn.setRequestProperty("Accept", "application/json");
		            
		            if (conn.getResponseCode() == 200) {
		            	
		            	InputStreamReader in = new InputStreamReader(conn.getInputStream());
			            BufferedReader br = new BufferedReader(in);
			            String output;
			            
			            while ((output = br.readLine()) != null) {
			            	
			                try {
			                	
								@SuppressWarnings("deprecation")
			                	JsonObject jsonObject = new JsonParser().parse(output).getAsJsonObject();
			                	
								String retornoValidaCpf = jsonObject.get("status").getAsString();
			                	
			                	if(!retornoValidaCpf.contains("UNABLE")) {
			                	
			                		LocalDateTime dataFim = LocalDateTime.parse(repoPauta.findById(voto.getPautaId().getId()).get().getDataFim().toString());
			                		LocalDateTime dataInicio = LocalDateTime.parse(repoPauta.findById(voto.getPautaId().getId()).get().getDataInicio().toString());
			                		
			                		LocalDateTime agora = LocalDateTime.now();
			                		
			                		if(agora.isAfter(dataInicio) && agora.isBefore(dataFim)) {
			                			
			                			if(repoVoto.validaSeVotou(
				                				repoCliente.findById(voto.getClienteId().getId()).get() , 
				                				repoPauta.findById(voto.getPautaId().getId()).get() 
				                				) == 0) {
				                			
				                			repoVoto.save(voto);
						    				
						        			retornoAPI = "{\"id\": \""+voto.getId().toString()+"\", \"mensagem\":\"Voto incluido com sucesso.\"}";
						        				
						        			retorno = new ResponseEntity<>(retornoAPI,HttpStatus.CREATED);
				                			
				                		} else {
						    				
						        			retornoAPI = "{\"erro\": \"CPF já votou\", \"mensagem\":\"Este CPF já votou nessa pauta.\"}";
						        				
						        			retorno = new ResponseEntity<>(retornoAPI,HttpStatus.CONFLICT);
				                		}
			                			
			                		} else {
			                			
			                			retornoAPI = "{\"erro\": \"Pauta encerrada\", \"mensagem\":\"Não é possivel votar nessa pauta.\"}";
				        				
					        			retorno = new ResponseEntity<>(retornoAPI,HttpStatus.CONFLICT);
			                		}
			                		
			                		
			                		
			                	} else {
			                		
				        			retornoAPI = "{\"erro\": \"CPF is "+retornoValidaCpf+"\", \"mensagem\":\"Este CPF não pode votar.\"}";
				        				
				        			retorno = new ResponseEntity<>(retornoAPI,HttpStatus.BAD_REQUEST);
			                	}
			                	
			                } catch (Exception e) {
			                	
			                	System.out.println("Erro api: "+ e.getMessage());
								
			                	retornoAPI = "{\"erro\": \""+e.getMessage()+"\", \"mensagem\":\"Erro ao incluir voto.\"}";
								
								retorno = new ResponseEntity<>(retornoAPI,HttpStatus.BAD_REQUEST);
							}
			               
			            }
		                
		            } else if (conn.getResponseCode() == 404) {
		            	
		            	retornoAPI = "{\"erro\": \"CPF inválido\", \"mensagem\":\"Voto não incluido. CPF inválido.\"}";
        				
	        			retorno = new ResponseEntity<>(retornoAPI,HttpStatus.BAD_REQUEST);
		            	
		            } else {
		            	
		            	throw new RuntimeException("Failed : HTTP Error code : "
		                        + conn.getResponseCode());
		            }
		            
		            conn.disconnect();

		        } catch (Exception e) {
		            
		            retornoAPI = "{\"erro\": \""+e.getMessage()+"\", \"mensagem\":\"Erro ao incluir voto.\"}";
					
					retorno = new ResponseEntity<>(retornoAPI,HttpStatus.BAD_REQUEST);
		        }
			
		}
		
		return retorno;
	}
}
